public class Card{
	//fields
	private String suits; // Hearts, Diamonds, Spades, Clubs
	private String value; // Ace, Two, Three... Ten, Jack, King, Queen
	
	//constructor
	public Card(String suits, String value){
		this.suits = suits;
		this.value = value;
	}
	
	//getters 
	public String getSuit(){
		return this.suits;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suits;
	}		
}