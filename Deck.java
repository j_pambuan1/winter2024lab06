import java.util.Random;

public class Deck {
	// fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;

	// getters
	public Card[] getCards() {
		return this.cards;
	}

	public int getNumberOfCards() {
		return this.numberOfCards;
	}

	public Random getRng() {
		return this.rng;
	}

	// constructor
	public Deck() {
		this.numberOfCards = 52;
		this.rng = new Random(); // remove Random in front of rng
		cards = new Card[numberOfCards]; // remove (Card[]) in front of cards

		String[] possibleSuits = new String[] { "Hearts", "Diamonds", "Spades", "Clubs" };
		String[] possibleValues = new String[] { "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
				"Ten", "Eleven", "Twelve", "Thirteen" };

		int counter = 0;
		for (int i = 0; i < possibleSuits.length; i++) {
			for (int j = 0; j < possibleValues.length; j++) {
				Card temp = new Card(possibleSuits[i], possibleValues[j]);
				cards[counter] = temp;
				counter++;
			}
		}

		// for (int i = 0; i < cards.length; i++) {
		// System.out.println(cards[i]);
		// }
	}

	// custom instance methods

	public int length() {
		return numberOfCards;
	}

	public Card drawTopCard() {
		if (numberOfCards > 1) {
			this.numberOfCards = this.numberOfCards - 1;
			Card[] temp = new Card[cards.length - 1];
			for (int i = 0; i < cards.length - 1; i++) {
				temp[i] = cards[i];
			}
			cards = temp;
		}
		return cards[this.numberOfCards - 1];

		// i removed -1 (supposed to return the card in the last position) here in the
		// [] and it doesn't throw anymore exceptions
	}

	public void shuffle() {
		for (int i = 0; i < this.numberOfCards; i++) {
			int rngNum = rng.nextInt(this.numberOfCards);
			Card temp = cards[i];
			this.cards[i] = cards[rngNum];
			cards[rngNum] = temp;
			// System.out.println(this.cards[i]); //keep this line to visual the SHUFFLING
		}
	}

	public String toString() {
		String deck = "";
		for (int i = 0; i < numberOfCards; i++) {
			deck = deck + this.cards[i] + "\n";
		}
		return deck;
	}

}