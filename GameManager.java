public class GameManager {
  private Deck drawPile;
  private Card centerCard;
  private Card playerCard;

  // getters
  public Card centerCard() {
    return this.centerCard;
  }

  public Card playerCard() {
    return this.playerCard;
  }

  public int getDrawPile() {
    return this.drawPile.length();
  }

  public GameManager() {
    Deck myDeck = new Deck();
    this.drawPile = myDeck;
    this.drawPile.shuffle();
    this.centerCard = drawPile.drawTopCard();
    this.playerCard = drawPile.drawTopCard();
  }

  public String toString() {
    String builder = "------------------------- \n Center card: " + this.centerCard
        + "\n Player card: " + this.playerCard + "\n-------------------------";
    return builder;
  }

  public void dealCards() {
    this.drawPile.shuffle();
    this.centerCard = drawPile.drawTopCard();
    this.playerCard = drawPile.drawTopCard();
  }

  public int getNumberOfCards() {
    return drawPile.getNumberOfCards();
  }

  public int calculatePoints() {
    if (centerCard.getValue() == playerCard.getValue()) {
      return 4;
    } else if (centerCard.getSuit() == playerCard.getSuit()) {
      return 2;
    } else {
      return -1;
    }
  }

}