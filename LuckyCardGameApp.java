import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 1;
		System.out.println("Welcome User!");

		while (manager.getDrawPile() > 1 && totalPoints < 5) {
			// print the rounds
			System.out.println("Round: " + round);
			System.out.println(manager);
			int userPoints = manager.calculatePoints();
			totalPoints += userPoints; // i update my points
			// print the points of the user after each round
			System.out.println("Your points after round " + round + ": " + userPoints);
			manager.dealCards();
			System.out.println("*****************************");
			round++;
		}
		if (totalPoints >= 5) {
			System.out.println("Player wins with : " + totalPoints + " points");
		} else {
			System.out.println("Player loses with: " + totalPoints + " point(s) because they ran out of cards");
		}

	}
}

